import { GoogleAnalyticsService } from './../../services/google-analytics.service';
import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/services/game.service';
import { Game } from 'src/app/interfaces/interfaces';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-goty',
  templateUrl: './goty.component.html',
  styleUrls: ['./goty.component.css']
})
export class GotyComponent implements OnInit {

  juegos: Game[] = [];

  constructor(private gameService: GameService, private gas : GoogleAnalyticsService,) { }

  ngOnInit(): void {
    /** Carga la data de nuestro servicio */
    this.gameService.getNominados().subscribe(
      resp => {
        console.log(resp);
        this.gas.sendEventGA("login", {
          method : "WITH_TOKEN"
        });
        this.juegos = resp;
      }
    )
    
    const performance = window.performance;
    const time = Math.round(performance.now()); 
    this.gas.sendEventGA("timing_complete", {
      name : "load",
      value : time,
      event_category : "JS Dependencies"
    })
  }
  /** Realiza un post a nuestro servicio para  incrementar el numero de votos */
  votarJ(juego: Game){
    console.log(juego);
    this.gameService.postVotos(juego.id)
    .subscribe( (resp: {ok: boolean, mensaje: string}) => {
      console.log(resp);
      if (resp.ok) {
        Swal.fire({
          title: 'Success!',
          text: resp.mensaje,
          icon: 'success',
          confirmButtonText: 'Cool'
        });
        this.gas.sendCustomEvent('votar', 'votosSuccess', 'click', juego.id);
        this.gas.sendEventGA("purchase", {
          currency : "MXM",
          value : 756.212,
          items : [{
            item_id: juego.id,
            item_name : juego.name,
            item_brand : "PSP4",
            quantity : 2,
            price : "150.0"
          }]
        });
      } else {
        Swal.fire({
          title: 'Error!',
          text: resp.mensaje,
          icon: 'error',
          confirmButtonText: 'Try again'
        });
        this.gas.sendCustomEvent('votar', 'votosFail', 'click', juego.id);
      }
    });
  }

}
