export const environment = {
  production: true,
  url : 'https://us-central1-graficas-online.cloudfunctions.net',
  firebase: {
    apiKey: 'AIzaSyCjdVEXC2o8_iK6xyFcFr-uHvrDQZhJ8Ec',
    authDomain: 'graficas-online.firebaseapp.com',
    databaseURL: 'https://graficas-online.firebaseio.com',
    projectId: 'graficas-online',
    storageBucket: 'graficas-online.appspot.com',
    messagingSenderId: '514115010630',
    appId: '1:514115010630:web:927280add044667b528a47',
    measurementId: 'G-ST18EH776H'
  },
  analyticsUrl : "https://www.googletagmanager.com/gtag/js?id=G-0CNNY4TCDW",
  idMedicion : "G-0CNNY4TCDW"
};
